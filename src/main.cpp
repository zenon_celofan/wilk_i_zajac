#include <stdio.h>
#include <stdlib.h>
#include "bluepill.h"
#include "stm32f10x.h"
#include "Led.h"
#include "Pin.h"
#include "delay.h"

Led led;
Pin piezo(PB11);


void sound_a(void);
void sound_b(void);
void sound_c(void);
void sound_d(void);
void sound_ok(void);


void main() {

	micros_init();
	millis_init();

	int d = 788;

	while (1) {

		for (u8 i = 0; i < 6; i++) {
			delay(d);
			sound_a();
		}
		delay(53);
		sound_ok();

		if (d > 10)
			d = d - 10;
		else
			d = 788;

		for (u8 i = 0; i < 6; i++) {
			delay(d);
			sound_b();
		}
		delay(53);
		sound_ok();

		if (d > 10)
			d = d - 10;
		else
			d = 788;

		for (u8 i = 0; i < 6; i++) {
			delay(d);
			sound_c();
		}
		delay(53);
		sound_ok();

		if (d > 10)
			d = d - 10;
		else
			d = 788;

		for (u8 i = 0; i < 6; i++) {
			delay(d);
			sound_d();
		}
		delay(53);
		sound_ok();

		if (d > 10)
			d = d - 10;
		else
			d = 788;


    }
}



void sound_a(void) {
	for (u8 i = 0; i < 12; i++) {
		for (u8 j = 0; j < 2; j++) {
			piezo.digital_write(1);
			delay_us(180);
			piezo.digital_write(0);
			delay_us(240);
		}
		delay_us(188);
	}
} //sound_a()



void sound_b(void) {
	for (u8 i = 0; i < 3; i++) {
		for (u8 j = 0; j < 8; j++) {
			piezo.digital_write(1);
			delay_us(240);
			piezo.digital_write(0);
			delay_us(240);
		}
		delay_us(124);
	}
} //sound_b()



void sound_c(void) {
	for (u8 i = 0; i < 3; i++) {
		for (u8 j = 0; j < 8; j++) {
			piezo.digital_write(1);
			delay_us(240);
			piezo.digital_write(0);
			delay_us(304);
		}
		delay_us(124);
	}
} //sound_c()



void sound_d(void) {
	for (u8 i = 0; i < 4; i++) {
		for (u8 j = 0; j < 8; j++) {
			piezo.digital_write(1);
			delay_us(180);
			piezo.digital_write(0);
			delay_us(180);
		}
		delay_us(124);
	}
} //sound_d()



void sound_ok(void) {
	for (u8 i = 0; i < 8; i++) {
		for (u8 j = 0; j < 8; j++) {
			piezo.digital_write(1);
			delay_us(480);
			piezo.digital_write(0);
			delay_us(480);
		}
		delay_us(124);
	}
} //sound_ok()
